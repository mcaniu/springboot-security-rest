# template spring security

## Proyecto plantilla de autentificacion spring security
Proyecto maqueta o plantilla para generar un rest api con autentificacion de usuario extraido de fuente
relizada con maven, a las que se les hicieron ciertas modificaciones especificadas en
el changelog.

Referencia del Ejemplo
Fuente Web: https://www.bezkoder.com/spring-boot-jwt-authentication/
Repositorio: https://github.com/bezkoder/spring-boot-spring-security-jwt-authentication


## Futuras mejoras
- modificacion url clase de seguridad
- adicion de swagger
- pruebas unitarias
- documentacion con javadoc
## Tecnologias aplicadas
- java 1.8
- springboot v2.7.2
- gradle-6.8
- jsonwebtoken jjwt 0.9.1(old)

