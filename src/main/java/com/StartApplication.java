package com;

import com.enums.ERole;
import com.entities.Role;
import com.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import static java.util.Arrays.asList;

@SpringBootApplication
public class StartApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }

    /**
     * Se ejecutan algunas inserciones en la tabla rol necesarias para el funcionamiento.
     */
    @Bean
    public CommandLineRunner run(RoleRepository roleRepository) throws Exception {
        // creacion de roles existentes
        return (String[] args) -> {
            Role rol1 = new Role(ERole.ROLE_USER);
            Role rol2 = new Role(ERole.ROLE_MODERATOR);
            Role rol3 = new Role(ERole.ROLE_ADMIN);

            roleRepository.saveAll(asList(rol1, rol2, rol3));
            roleRepository.findAll().forEach(role -> System.out.println(role));
        };
    }
}
