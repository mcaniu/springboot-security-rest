package com.utils;

public class AppUtil {
    public static String REGISTRO_EXITOSO = "Usuario registrado exitosamente!";
    public static String ROL_NO_ENCONTRADO = "Error: Rol no Encontrado";
    public static String EMAIL_EXISTENTE = "Error: Email esta en uso";
    public static String USUARIO_EXISTENTE = "Error: Username esta en uso";
    public static String[] URLS_PUBLICAS = {"/api/test/**", "/api/auth/**", "/v3/api-docs/**", "/swagger-ui/**"};


}
