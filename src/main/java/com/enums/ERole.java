package com.enums;

/**
 * Enumerador que haran match con lo almacenado en base de datos.
 */
public enum ERole {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN
}
