package com.dtos.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;


/**
 * DTO que transportaran los datos para el ingreso del usuario.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {
    @NotBlank(message = "username no debe estar vacio")
    @Size(min = 3, max = 20, message = "tamaño maximo es hasta 50 caracteres y tamaño minimo es desde 3 caracteres")
    private String username;

    @NotBlank(message = "email no debe estar vacio")
    @Size(max = 50, message = "tamaño maximo es hasta 50 caracteres")
    @Email
    private String email;

    private Set<String> role;

    @NotBlank(message = "password no debe estar vacia")
    @Size(min = 6, max = 40, message = "tamaño maximo es hasta 50 caracteres")
    private String password;

}
