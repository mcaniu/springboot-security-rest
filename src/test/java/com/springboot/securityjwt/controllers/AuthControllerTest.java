package com.springboot.securityjwt.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.securityjwt.dtos.request.LoginRequest;
import com.springboot.securityjwt.dtos.request.SignupRequest;
import com.springboot.securityjwt.entities.Role;
import com.springboot.securityjwt.enums.ERole;
import com.springboot.securityjwt.repository.RoleRepository;
import com.springboot.securityjwt.repository.UserRepository;
import com.springboot.securityjwt.security.jwt.JwtUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@WebMvcTest(AuthController.class) // se indica que clase se utilizara para el test
@AutoConfigureMockMvc(addFilters = false) // sin esto respondia 403
public class AuthControllerTest {

    // clases emuladoras de los llamados y mapeador de objeto java
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    // clases inyectadas para emulacion del llamado de la clase a testear
    @MockBean
    AuthenticationManager authenticationManager;

    @MockBean
    UserRepository userRepository;

    @MockBean
    RoleRepository roleRepository;

    @MockBean
    PasswordEncoder encoder;

    @MockBean
    JwtUtils jwtUtils;


    @Test
    void registrarUsuario() throws Exception {
        Set<String> listadoRoles = new HashSet<>();
        listadoRoles.add("mod");
        SignupRequest signupRequest = new SignupRequest("pepito", "pepito@gmail.com", listadoRoles, "123456789");

        when(roleRepository.findByName(ERole.ROLE_MODERATOR)).thenReturn(Optional.of(new Role(ERole.ROLE_MODERATOR)));

        mockMvc.perform(post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signupRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

    @Test
    void autentificacion() throws Exception {
        LoginRequest loginRequest = new LoginRequest("pepito", "123456789");

        when(roleRepository.findByName(ERole.ROLE_MODERATOR)).thenReturn(Optional.of(new Role(ERole.ROLE_MODERATOR)));

        mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

}
