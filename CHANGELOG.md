## [1.0.0] - 2022-08-29
### Modificado
- se crea variable estatica para las urls publicas configuradas del spring security

## [1.0.0] - 2022-08-25
### Agregado
- se agrega libreria de OpenApi
- se agregan algunos comentarios javadoc 
- se agrega clase para el testing de LoginController.registrarUsuario


## [1.0.0] - 2022-08-23
### Modificado
- se cambia la configuracion de spring security (WebSecurityConfigurerAdapter deprecated)

## [1.0.0] - 2022-08-22
### Modificado
- se agregan anotaciones lombok para clases entities y dtos
- se renombran algunos packages
- se agrega clase utilitaria

## [1.0.0] - 2022-08-18
### Modificado
- se crea carpeta con documentacion con ejemplos de la fuente original
- se cambia datasource hacia bd h2
- se agrega acceso conexion con h2 via codigo 
- se elimina referencia hacia otros datasources
- creacion de roles via clase CommandLineRunner
- creacion automatica de base de datos
- pasar de maven a gradle
- eliminar git de fuente original y traspasar a repositorio propio